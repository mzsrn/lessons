require 'json'
require 'byebug'

class MyMiddleware
  def initialize appl
    @appl = appl
  end

  def call env
    request = Rack::Request.new(env)
    body = {
      path: request.path,
      verb: request.request_method,
      ip: request.ip,
      cookies: request.cookies,
      params: request.params,
      body: JSON.parse(request.body.read)
    }
    [200, {}, [body.to_json]]
  end
end

class App
  def call env
  end
end

use MyMiddleware
run App.new

# rackup
# curl -X POST localhost:9292/users?sort=desc -d "{\"login\":\"admin\",\"password\":\"password\"}"