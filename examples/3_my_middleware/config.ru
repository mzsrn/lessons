class MyMiddleware
  def initialize appl
    @appl = appl
  end

  def call env
    status, headers, body = @appl.call(env)
    request = Rack::Request.new(env)
    if request.path == "/"
      case request.request_method
      when "GET"
        [status, headers, body]
      when "POST"
        [201, headers.merge({"x-created" => "True"}), ["Item was successfuly created"]]
      end
    else
      [404, {}, ["Not Found"]]
    end
  end
end

class App
  def call env
    [200, {}, ["Success"]]
  end
end

use MyMiddleware
run App.new

# rackup