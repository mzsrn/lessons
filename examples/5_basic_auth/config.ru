use Rack::Auth::Basic do |username, password|
  username == 'admin' && password == 'password'
end

class App

  def call env
    puts env["HTTP_AUTHORIZATION"]
    return [200, {"Content-Type" => "text/html"},  ['You are logged in successfully']]
  end

end
run App.new

# rackup
# curl -u admin:password -i localhost:9292