require 'minitest/autorun'
require 'rack/test'

class MyApp

  def call env
    [200, {"X-Success": true}, ["Success response"]]
  end

end

describe "MyApp" do
  include Rack::Test::Methods

  def app
    MyApp.new
  end

  it "check response status" do
    get "/"
    assert last_response.ok?
  end

  it "check response headers" do
    get "/"
    assert_equal last_response.headers, {"X-Success": true} 
  end

  it "check response body" do
    get "/"
    assert_equal last_response.body, "Success response1"
  end
  
end

# ruby test.rb

## Полный список методов ~/.rvm/gems/ruby-3.0.1/gems/rack-test-1.1.0/lib/rack/test/methods.rb

---
# HOMEWORK: 
# hexlet program download rails rack