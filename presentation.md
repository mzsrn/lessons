---
theme: gaia
class:
  - lead
  - invert
paginate: true
---
# Rack

---
# Что обсудим:

* HTTP
* Rack
* Middlewares

---
<!-- header: "HTTP" -->
![](images/http.png)

---
# HTTP Request

* Verb (GET, POST, PUT, etc)
* Path (https://ru.hexlet.io)
* Headers
* Parameters (/courses/42?step=2)

---
# HTTP Response
* Status (1xx, 2xx, 3xx, 4xx, 5xx)
* Headers
* Body

---
# Пример
```shell
% curl --head https://ru.hexlet.io
```

```
< HTTP/2 200
< server: nginx/1.19.6
< date: Mon, 19 Apr 2021 17:35:13 GMT
< content-type: text/html; charset=utf-8
< x-xss-protection: 1; mode=block
< referrer-policy: strict-origin-when-cross-origin
...
```

---
# Пример
```shell
% curl -X GET https://ru.hexlet.io
```

```
<!DOCTYPE html>
<html lang='ru' prefix='og: https://ogp.me/ns#'>
  <head>
    <title>Обучение программированию: онлайн-курсы для новичков и опытных</title>
    <meta name="csrf-param" content="authenticity_token" />
    <meta name="csrf-token" 
...
```

---
<!-- header: '' -->
![width:400px bg left:35%](images/rack_logo.png)

* Стандарт интерфейса веб-сервера
* Каркас для Middlewares
* Gem

---
<!-- header: 'Rack: Стандарт интерфейса веб-сервера' -->

```ruby
> Rack::Handler::Puma
> Rack::Handler::Thin
> Rack::Handler::WEBrick # default
```

---
* irb | ruby my_app.rb
  ```ruby
  require 'rack'
  Rack::Handler::Thin.run lambda { |env| [200, {"Content-Type" => "text/plain"}, ["Hello. The time is #{Time.now}"]] }
  ```

* config.ru
  ```shell
  % rackup
  % curl -X GET localhost:9292
  Hello. The time is 2021-04-19 21:07:48 +0300%
  ```

---
<!-- header: 'Rack: Каркас для Middlewares' -->
![](images/middleware.png) 

---
<!-- header: 'Middleware' -->

```ruby
class MyMiddleware
  def initialize appl
    @appl = appl
  end

  def call env
    status, headers, body = @appl.call(env)
  end
end

class App
  def call env
    [200, {}, ["Success"]]
  end
end

use MyMiddleware
run App.new
```

---
# Назначение
* Авторизация
* Мониторинг
* Логирование
* Сериализация
* Роутинг
* Бизнес-логика

---
<!-- header: 'Rack: Gem' -->

## Rack Middlewares
* `Rack::Files`
* `Rack::Events`
* `Rack::Head`
* `Rack::Lock`
* `Rack::Reloader`
* `Rack::Runtime`
* `Rack::ShowException`
---

## Rack Helpers
* `Rack::Request`
* `Rack::Response`
* `Rack::MockRequest` и `Rack::MockResponse`
* `Rack::MediaType`
* `Rack::Mime`
* `Rack::RewindableInput`

---
# HOMEWORK: 
```
hexlet program download rails rack
```