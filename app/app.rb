require 'sinatra'

get '/' do
  [200, {}, ["Success"]]
end

post '/' do
  [201, {"X-Created" => "True"}, ["Item was successfuly created"]]
end