require_relative 'app'
require_relative 'my_middleware'
use Rack::Reloader
use MyMiddleware

run App.new