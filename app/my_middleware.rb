class MyMiddleware

  def initialize appl
    @appl = appl
  end

  def call env
    status, headers, body = @appl.call(env)
    request = Rack::Request.new(env)
    if request.path == "/"
      if request.request_method == "GET"
        return [status, headers, body]
      elsif request_method == "POST"
        # create_item
        new_status = 201
        new_headers = headers.merge({"X-Created" => "True"})
        new_body = "Item was successfuly created"
        [new_status, new_headers, [new_body]]
      end
    else
      [200, {}, ["Not Found"]]
    end
  end

end